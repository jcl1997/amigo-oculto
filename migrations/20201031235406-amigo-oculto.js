'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface
      .sequelize
      .query(
        `
        ALTER TABLE user 
        ADD COLUMN created_at DATETIME NOT NULL AFTER password,
        ADD COLUMN created_update DATETIME NOT NULL AFTER created_at

        `
      );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};