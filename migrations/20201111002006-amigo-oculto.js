'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface
      .sequelize
      .query(
        `
        ALTER TABLE user
	      CHANGE COLUMN password password VARCHAR(245) NOT NULL COLLATE 'utf8_general_ci' AFTER email;
        `
      );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};