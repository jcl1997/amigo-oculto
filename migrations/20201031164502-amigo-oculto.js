'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface
      .sequelize
      .query(
        `
        CREATE TABLE IF NOT EXISTS user (
          id INT(11) NOT NULL AUTO_INCREMENT,
          name VARCHAR(45) NOT NULL,
          email VARCHAR(45) NOT NULL,
          password VARCHAR(45) NOT NULL,
          PRIMARY KEY (id),
          UNIQUE INDEX id_UNIQUE (id ASC),
          UNIQUE INDEX email_UNIQUE (email ASC)
        )
        ENGINE = InnoDB
        DEFAULT CHARACTER SET = utf8    
        `
      );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};