'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface
      .sequelize
      .query(
        `
        ALTER TABLE user
        CHANGE COLUMN created_at createdAt DATETIME NOT NULL AFTER password,
        CHANGE COLUMN created_update updatedAt DATETIME NOT NULL AFTER createdAt;
        `
      )
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
