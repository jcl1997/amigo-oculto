"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.remove = exports.get = exports.edit = exports.validationFormUserEdit = exports.create = exports.validationFormUser = void 0;
const express_validator_1 = require("express-validator");
const password_1 = __importDefault(require("../token/password"));
const User_1 = __importDefault(require("../../models/User"));
const hasEmail_1 = __importDefault(require("../hasEmail"));
const getUser_1 = __importDefault(require("../getUser"));
exports.validationFormUser = [
    express_validator_1.body("name").notEmpty().withMessage("O nome é obrigatório."),
    express_validator_1.body("email").notEmpty().withMessage("O email é obrigatório.").isEmail().withMessage("Informe um email válido."),
    express_validator_1.body("password")
        .notEmpty()
        .withMessage("Senha obrigatória.")
        .isLength({ min: 8, max: 20 })
        .withMessage("Informe uma senha com pelo menos 8 caracteres e no máximo 20."),
];
async function create(req, res) {
    const errors = express_validator_1.validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    try {
        const used = await hasEmail_1.default({
            id: null,
            email: req.body.email
        });
        if (used) {
            return res
                .status(422)
                .send({ error: "Email já em uso." });
        }
        const newUser = await User_1.default.create(Object.assign({}, req.body, {
            password: password_1.default(req.body.password)
        }));
        return res
            .status(200)
            .json({
            id: newUser.id,
            name: newUser.name,
            email: newUser.email,
        });
    }
    catch (error) {
        // tslint:disable-next-line:no-console
        console.log(error);
        return res
            .status(422)
            .send({ error: "Houve um error." });
    }
}
exports.create = create;
exports.validationFormUserEdit = [
    express_validator_1.body("name").notEmpty().withMessage("O nome é obrigatório."),
    express_validator_1.body("email").notEmpty().withMessage("O email é obrigatório.").isEmail().withMessage("Informe um email válido."),
];
async function edit(req, res) {
    if (!req.isAuthenticated()) {
        return res.redirect('/login');
    }
    const errors = express_validator_1.validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    try {
        const { id } = req.params;
        const user = await getUser_1.default({ id });
        if (!user) {
            return res
                .status(422)
                .send({ error: "Usuário não encontrado." });
        }
        const used = await hasEmail_1.default({
            id,
            email: req.body.email
        });
        if (used) {
            return res
                .status(422)
                .send({ error: "Email já em uso." });
        }
        await user.update(req.body, {
            where: {
                id: req.params.id
            },
        });
        return res
            .status(200)
            .json({ msg: "sucesso" });
    }
    catch (error) {
        return res
            .status(422)
            .send({ error: "Houve um error." });
    }
}
exports.edit = edit;
async function get(req, res) {
    if (!req.isAuthenticated()) {
        return res.redirect('/login');
    }
    try {
        const users = await User_1.default.findAll({
            attributes: [
                'id',
                'name',
                'email',
            ]
        });
        return res
            .status(422)
            .send({ users });
    }
    catch (error) {
        return res
            .status(422)
            .send({ error: "Houve um error." });
    }
}
exports.get = get;
async function remove(req, res) {
    if (!req.isAuthenticated()) {
        return res.redirect('/login');
    }
    try {
        const { id } = req.params;
        const user = await getUser_1.default({ id });
        if (!user) {
            return res
                .status(422)
                .send({ error: "Usuário não encontrado." });
        }
        await User_1.default.destroy({
            where: { id }
        });
        return res
            .status(200)
            .json({ msg: "sucesso" });
    }
    catch (error) {
        return res
            .status(422)
            .send({ error: "Houve um error." });
    }
}
exports.remove = remove;
//# sourceMappingURL=UserController.js.map