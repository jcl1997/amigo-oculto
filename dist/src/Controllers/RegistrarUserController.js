"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.home = exports.singup = exports.singupForm = exports.validationFormSingup = exports.forgot = exports.forgotForm = exports.validationFormForgot = void 0;
const express_validator_1 = require("express-validator");
const formError_1 = __importDefault(require("../formError"));
const password_1 = __importDefault(require("../token/password"));
const User_1 = __importDefault(require("../../models/User"));
const hasEmail_1 = __importDefault(require("../hasEmail"));
exports.validationFormForgot = [
    express_validator_1.body("email").notEmpty().withMessage("O nome é obrigatório.")
];
function forgotForm(req, res) {
    return res.render('forgot.html.twig');
}
exports.forgotForm = forgotForm;
function forgot(req, res) {
    const { email } = req.body;
    const errors = express_validator_1.validationResult(req);
    if (!errors.isEmpty()) {
        return res.render('forgot.html.twig', {
            email,
            error: {
                messageKey: errors.array()[0].msg,
            }
        });
    }
    return res.render('forgot.html.twig', {
        email,
        error: {
            messageKey: 'Não implementado',
        }
    });
}
exports.forgot = forgot;
exports.validationFormSingup = [
    express_validator_1.body("name").notEmpty().withMessage("O nome é obrigatório."),
    express_validator_1.body("email").notEmpty().withMessage("O email é obrigatório.").isEmail().withMessage("Informe um email válido."),
    express_validator_1.body("password")
        .notEmpty()
        .withMessage("Senha obrigatória.")
        .isLength({ min: 8, max: 20 })
        .withMessage("Informe uma senha com pelo menos 8 caracteres e no máximo 20."),
];
function singupForm(req, res) {
    return res.render('singup.html.twig');
}
exports.singupForm = singupForm;
async function singup(req, res) {
    const { name, email } = req.body;
    const errors = express_validator_1.validationResult(req);
    if (!errors.isEmpty()) {
        return res.render('singup.html.twig', {
            name,
            email,
            error: {
                messageKey: "Houve um erro, verifique o formulário.",
            },
            form: formError_1.default(errors.array()),
        });
    }
    try {
        const used = await hasEmail_1.default({
            id: null,
            email: req.body.email
        });
        if (used) {
            return res
                .status(422)
                .render('singup.html.twig', {
                name,
                email,
                error: {
                    messageKey: "Email já em uso.",
                },
            });
        }
        const newUser = await User_1.default.create(Object.assign({}, req.body, {
            password: password_1.default(req.body.password)
        }));
        return res.render('login.html.twig', {
            email,
        });
    }
    catch (error) {
        // tslint:disable-next-line:no-console
        console.log(error);
        return res
            .status(422)
            .render('singup.html.twig', {
            name,
            email,
            error: {
                messageKey: "Houve um error.",
            },
        });
    }
}
exports.singup = singup;
function home(req, res) {
    if (!req.isAuthenticated()) {
        return res.redirect('/login');
    }
    return res.render('home.html.twig', {
        id: req.user.id,
        name: req.user.name,
        email: req.user.email,
    });
}
exports.home = home;
//# sourceMappingURL=RegistrarUserController.js.map