"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUserByEmail = void 0;
const User_1 = __importDefault(require("../models/User"));
const getUser = async ({ id }) => {
    const user = await User_1.default
        .findOne({
        where: { id }
    });
    return user;
};
const getUserByEmail = async ({ email }) => {
    const user = await User_1.default
        .findOne({
        where: { email }
    });
    return user;
};
exports.getUserByEmail = getUserByEmail;
exports.default = getUser;
//# sourceMappingURL=getUser.js.map