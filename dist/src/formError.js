"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (errors) => {
    return errors.reduce((acc, cur) => {
        if (Object.keys(acc).includes(cur.param)) {
            acc[`${cur.param}`] = [...acc[`${cur.param}`], {
                    error: `${cur.msg}`
                }];
            return acc;
        }
        return Object.assign({}, acc, {
            [`${cur.param}`]: [{
                    error: `${cur.msg}`
                }]
        });
    }, {});
};
//# sourceMappingURL=formError.js.map