"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv");
const path_1 = __importDefault(require("path"));
const express_1 = __importDefault(require("express"));
const routes_1 = __importDefault(require("./routes"));
const uuid_1 = require("uuid");
const express_session_1 = __importDefault(require("express-session"));
// tslint:disable-next-line:no-var-requires
const FileStore = require('session-file-store')(express_session_1.default);
const express_validator_1 = require("express-validator");
const body_parser_1 = __importDefault(require("body-parser"));
const passport_1 = __importDefault(require("passport"));
const passport_local_1 = require("passport-local");
const getUser_1 = __importStar(require("./getUser"));
const authenticated_1 = __importDefault(require("./token/authenticated"));
passport_1.default.use('local', new passport_local_1.Strategy({ usernameField: 'email' }, async (email, password, done) => {
    try {
        const user = await getUser_1.getUserByEmail({ email });
        if (!user) {
            return done(null, false, { message: 'Email ou senha inválido.' });
        }
        if (!authenticated_1.default(user.password, password)) {
            return done(null, false, { message: 'Email ou senha inválido.' });
        }
        return done(null, user);
    }
    catch (error) {
        return done(null, false, { message: 'Houve um erro.' });
    }
}));
passport_1.default.serializeUser((user, done) => {
    done(null, user.id);
});
passport_1.default.deserializeUser(async (id, done) => {
    const user = await getUser_1.default({ id: Number(id) });
    if (!user) {
        return done('Usuário não encontrado.', false);
    }
    done(null, user);
});
const app = express_1.default();
const port = process.env.PORT || 3000;
app.use(express_1.default.urlencoded({
    extended: false
}));
app.use(express_1.default.json());
app.use(express_session_1.default({
    genid: (req) => {
        return uuid_1.v4();
    },
    store: new FileStore({}),
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true
}));
app.use(body_parser_1.default.urlencoded({ extended: false }));
app.use(body_parser_1.default.json());
app.use(passport_1.default.initialize());
app.use(passport_1.default.session());
app.set("views", path_1.default.join(__dirname, "views"));
app.set("view engine", "twig");
app.use('/static', express_1.default.static(path_1.default.join(__dirname, "static")));
routes_1.default.get('/login', (req, res) => {
    res.render('login.html.twig');
});
routes_1.default.post('/login', [
    express_validator_1.body("email").notEmpty().withMessage("O email é obrigatório."),
    express_validator_1.body("password").notEmpty().withMessage("A senhar é obrigatório.")
], async (req, res, next) => {
    const { email } = req.body;
    const errors = express_validator_1.validationResult(req);
    if (!errors.isEmpty()) {
        return res.render('login.html.twig', {
            email,
            error: {
                messageKey: errors.array()[0].msg,
            }
        });
    }
    passport_1.default.authenticate('local', (err, user, info) => {
        if (info) {
            return res.render('login.html.twig', {
                email,
                error: {
                    messageKey: info.message,
                }
            });
        }
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.redirect('/login');
        }
        req.login(user, (err1) => {
            if (err1) {
                return next(err1);
            }
            return res.redirect('/');
        });
    })(req, res, next);
});
app.use("/", routes_1.default);
app.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log("Sample mySQL app listening on port " + port);
});
//# sourceMappingURL=index.js.map