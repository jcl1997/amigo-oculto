"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const User_1 = __importDefault(require("../models/User"));
const hasEmail = async ({ id, email }) => {
    const user = await User_1.default
        .findOne({
        where: {
            [sequelize_1.Op.and]: [
                { email },
                { id: { [sequelize_1.Op.ne]: id } }
            ]
        }, attributes: [
            'id',
            'name',
            'email',
        ]
    });
    return user ? true : false;
};
exports.default = hasEmail;
//# sourceMappingURL=hasEmail.js.map