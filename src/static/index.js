var nome = document.getElementById('validationServer03');
var idade = document.getElementById('validationServer04');
var salario = document.getElementById('inlineFormInputGroupUsername');
if (salario.value) {
    var valor = numeral(salario.value).format('0000.00');
    valor = valor.replace('.', ',');
    salario.value = valor;
}

function formatReal() {
    if (salario.value) {
        var valor = numeral(salario.value).format('0000.00');
        valor = valor.replace('.', ',');
        salario.value = valor;
    }
}

function salvar() {
    console.log({
        "nome": nome.value,
        "idade": idade.value,
        "salario": salario.value.replace(',', '.'),
    });
}