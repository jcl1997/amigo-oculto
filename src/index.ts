import "dotenv";
import path from "path";
import express from "express";
import routes from "./routes";
import { v4 } from "uuid";
import session from "express-session";
// tslint:disable-next-line:no-var-requires
const FileStore = require('session-file-store')(session);
import { body, validationResult } from "express-validator";
import bodyParser from 'body-parser'
import User from "../models/User";
import passport from "passport";
import { Strategy as LocalStrategy} from "passport-local";
import getUser, {  getUserByEmail } from "./getUser";
import auth  from './token/authenticated';

passport.use('local', new LocalStrategy(
    { usernameField: 'email' },
    async (email, password, done) => {
      try {
        const user: User = await getUserByEmail({ email });
        if (!user) {
          return done(null, false, { message: 'Email ou senha inválido.' });
        }

        if (!auth(user.password, password)) {
          return done(null, false, { message: 'Email ou senha inválido.' });
        }

        return done(null, user);
      } catch (error) {
        return done(null, false, { message: 'Houve um erro.' });
      }
    }
));

passport.serializeUser((user: any, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  const user: User = await getUser({ id: Number(id) });
  if (!user) {
    return done('Usuário não encontrado.', false);
  }

  done(null, user);
});

const app = express();
const port = process.env.PORT || 3000;

app.use(express.urlencoded({
  extended: false
}))
app.use(express.json());
app.use(session({
  genid: (req) => {
    return v4();
  },
  store: new FileStore({}),
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: true
}));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(passport.initialize());
app.use(passport.session());

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "twig");
app.use('/static', express.static(path.join(__dirname, "static")));

routes.get('/login', (req: any, res: any) => {
  res.render('login.html.twig');
});

routes.post(
  '/login',
  [
    body("email").notEmpty().withMessage("O email é obrigatório."),
    body("password").notEmpty().withMessage("A senhar é obrigatório.")
  ],
  async (req: any, res: any, next: any) => {
    const { email } = req.body;
    const errors: any = validationResult(req);

    if (!errors.isEmpty()) {
      return res.render('login.html.twig', {
        email,
        error: {
          messageKey: errors.array()[0].msg,
        }
      });
    }

    passport.authenticate('local', (err, user, info) => {
      if (info) {
        return res.render('login.html.twig', {
          email,
          error: {
            messageKey: info.message,
          }
        });
      }

      if (err) {
        return next(err);
      }

      if (!user) {
        return res.redirect('/login');
      }

      req.login(user, (err1: any) => {
        if (err1) {
          return next(err1);
        }

        return res.redirect('/');
      })
    })(req, res, next);
});

app.use("/", routes);

app.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log("Sample mySQL app listening on port " + port);
});
