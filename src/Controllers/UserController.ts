import { body, validationResult } from "express-validator";
import jwtPassword from "../token/password";
import User from "../../models/User";
import hasEmail from '../hasEmail';
import getUser from "../getUser";

export const validationFormUser = [
  body("name").notEmpty().withMessage("O nome é obrigatório."),
  body("email").notEmpty().withMessage("O email é obrigatório.").isEmail().withMessage("Informe um email válido."),
  body("password")
    .notEmpty()
    .withMessage("Senha obrigatória.")
    .isLength({ min: 8, max: 20 })
    .withMessage(
      "Informe uma senha com pelo menos 8 caracteres e no máximo 20."
    ),
];
export async function create(req: any, res: any) {
  const errors: any = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  try {
    const used = await hasEmail({
      id: null,
      email: req.body.email
    });

    if (used) {
      return res
        .status(422)
        .send({ error: "Email já em uso." });
    }

    const newUser = await User.create(Object.assign({}, req.body, {
      password: jwtPassword(req.body.password)
    }));

    return res
      .status(200)
      .json({
        id: newUser.id,
        name: newUser.name,
        email: newUser.email,
      });
  } catch (error) {
    // tslint:disable-next-line:no-console
    console.log(error);
    return res
      .status(422)
      .send({ error: "Houve um error." });
  }
}

export const validationFormUserEdit = [
  body("name").notEmpty().withMessage("O nome é obrigatório."),
  body("email").notEmpty().withMessage("O email é obrigatório.").isEmail().withMessage("Informe um email válido."),
];

export async function edit(req: any, res: any) {
  if(!req.isAuthenticated()) {
    return res.redirect('/login');
  }

  const errors: any = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  try {
    const { id } = req.params;
    const user = await getUser({ id });

    if (!user) {
      return res
        .status(422)
        .send({ error: "Usuário não encontrado." });
    }

    const used = await hasEmail({
      id,
      email: req.body.email
    });

    if (used) {
      return res
        .status(422)
        .send({ error: "Email já em uso." });
    }

    await user.update(req.body, {
      where: {
        id: req.params.id
      },
    })

    return res
      .status(200)
      .json({ msg: "sucesso" });
  } catch (error) {
    return res
      .status(422)
      .send({ error: "Houve um error." });
  }
}

export async function get(req: any, res: any) {
  if(!req.isAuthenticated()) {
    return res.redirect('/login');
  }

  try {
    const users = await User.findAll({
      attributes: [
        'id',
        'name',
        'email',
      ]
    })
    return res
      .status(422)
      .send({ users });
  } catch (error) {
    return res
      .status(422)
      .send({ error: "Houve um error." });
  }
}

export async function remove(req: any, res: any) {
  if(!req.isAuthenticated()) {
    return res.redirect('/login');
  }

  try {
    const { id } = req.params;
    const user = await getUser({ id });

    if (!user) {
      return res
        .status(422)
        .send({ error: "Usuário não encontrado." });
    }

    await User.destroy({
      where: { id }
    });

    return res
      .status(200)
      .json({ msg: "sucesso" });
  } catch (error) {
    return res
      .status(422)
      .send({ error: "Houve um error." });
  }
}