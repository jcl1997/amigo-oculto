import { body, validationResult } from "express-validator";
import formError from '../formError'
import jwtPassword from "../token/password";
import User from "../../models/User";
import hasEmail from '../hasEmail';
import { log } from "console";

export const validationFormForgot = [
    body("email").notEmpty().withMessage("O nome é obrigatório.")
];

export function forgotForm(req: any, res: any) {
    return res.render('forgot.html.twig');
}

export function forgot(req: any, res: any) {
    const { email } = req.body;
    const errors: any = validationResult(req);

      if (!errors.isEmpty()) {
        return res.render('forgot.html.twig', {
          email,
          error: {
            messageKey: errors.array()[0].msg,
          }
        });
      }

    return res.render('forgot.html.twig', {
      email,
      error: {
        messageKey: 'Não implementado',
      }
    });
}

export const validationFormSingup = [
    body("name").notEmpty().withMessage("O nome é obrigatório."),
    body("email").notEmpty().withMessage("O email é obrigatório.").isEmail().withMessage("Informe um email válido."),
    body("password")
      .notEmpty()
      .withMessage("Senha obrigatória.")
      .isLength({ min: 8, max: 20 })
      .withMessage(
        "Informe uma senha com pelo menos 8 caracteres e no máximo 20."
      ),
];
export function singupForm(req: any, res: any) {
    return res.render('singup.html.twig');
}

export async function singup(req: any, res: any) {
    const { name, email } = req.body;
    const errors: any = validationResult(req);

    if (!errors.isEmpty()) {
        return res.render('singup.html.twig', {
            name,
            email,
            error: {
                messageKey: "Houve um erro, verifique o formulário.",
            },
            form: formError(errors.array()),
        });
    }

    try {
        const used = await hasEmail({
            id: null,
            email: req.body.email
        });

        if (used) {
            return res
                .status(422)
                .render('singup.html.twig', {
                    name,
                    email,
                    error: {
                        messageKey: "Email já em uso.",
                    },
                });

        }

        const newUser = await User.create(Object.assign({}, req.body, {
            password: jwtPassword(req.body.password)
        }));

        return res.render('login.html.twig', {
            email,
        });
    } catch (error) {
        // tslint:disable-next-line:no-console
        console.log(error);
        return res
            .status(422)
            .render('singup.html.twig', {
                name,
                email,
                error: {
                    messageKey: "Houve um error.",
                },
            });
  }
}

export function home(req: any, res: any) {
    if(!req.isAuthenticated()) {
        return res.redirect('/login');
    }

    return res.render('home.html.twig', {
        id: req.user.id,
        name: req.user.name,
        email: req.user.email,
    });
}