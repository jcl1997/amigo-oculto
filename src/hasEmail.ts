import { Op } from "sequelize";
import User from "../models/User";

interface IProps {
    id?: number;
    email: string;
}

const hasEmail = async ({ id, email }: IProps): Promise<boolean> => {
    const user = await User
        .findOne({
            where: {
                [Op.and]: [
                    { email },
                    { id: { [Op.ne]: id } }
                ]
            }, attributes:
                [
                    'id',
                    'name',
                    'email',
                ]
        });

    return user ? true : false;
}

export default hasEmail;