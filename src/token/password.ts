import "dotenv";
import * as JWT from "jsonwebtoken";

export default function token(password: string) {
    return JWT
        .sign(
            password,
            process.env.JWT_PASSWORD
        );
}