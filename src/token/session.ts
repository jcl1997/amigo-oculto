import "dotenv";
import * as JWT from "jsonwebtoken";

interface IUser {
    id: number;
    name: string;
    email: string;
}

export function session({
    id,
    name,
    email,
}: IUser) {
    return JWT.sign(
        {
            id,
            name,
            email,
        },
        process.env.JWT_PASSWORD,
        { expiresIn: '2h' }
    );
}
