import "dotenv";
import * as JWT from "jsonwebtoken";

export default function user(token: string, password: string) {
    const decode = JWT.verify(token, process.env.JWT_PASSWORD);
    return decode === password;
};
