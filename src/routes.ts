import express from "express";
import * as UserController from "./Controllers/UserController";
import * as RegistrarUserController from './Controllers/RegistrarUserController';

const routes = express.Router();

routes.get("/", RegistrarUserController.home);

routes.get("/forgot", RegistrarUserController.forgotForm);
routes.post("/forgot", RegistrarUserController.validationFormForgot, RegistrarUserController.forgot);

routes.get("/singup", RegistrarUserController.singupForm);
routes.post("/singup", RegistrarUserController.validationFormSingup, RegistrarUserController.singup);

routes.post("/user", UserController.validationFormUser, UserController.create);
routes.put("/user/:id", UserController.validationFormUserEdit, UserController.edit);
routes.get("/user", UserController.get);
// routes.delete("/user/:id", UserController.remove);

routes.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});
export default routes;
