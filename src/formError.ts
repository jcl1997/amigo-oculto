interface IValidationResult {
    msg: string;
    param: string;
    value: string;
    location: string;
    nestedErrors?: any;
}

export default (errors: IValidationResult[]) => {
    return errors.reduce((acc: any, cur: IValidationResult) => {
        if (Object.keys(acc).includes(cur.param)) {
            acc[`${cur.param}`] = [...acc[`${cur.param}`], {
                error: `${cur.msg}`
            }];

            return acc;
        }

        return Object.assign({}, acc, {
            [`${cur.param}`]: [{
                error: `${cur.msg}`
            }]
        })
    }, {});
}