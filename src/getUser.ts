import User from "../models/User";

interface IProp {
    id: number;
}

interface IEmail {
    email: string;
}

const getUser = async ({ id }: IProp): Promise<User> => {
    const user = await User
        .findOne({
            where: { id }
        });
    return user;
}

export const getUserByEmail = async ({ email }: IEmail): Promise<User> => {
    const user = await User
        .findOne({
            where: { email }
        });
    return user;
}

export default getUser;