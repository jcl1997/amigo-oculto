import { Model, DataTypes } from "sequelize";
import sequelize from "./database";

class User extends Model {
  public id!: number; // Note that the `null assertion` `!` is required in strict mode.
  public name!: string;
  public email!: string;
  public password: string;
}

User.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: new DataTypes.STRING(45),
      allowNull: false,
    },
    email: {
      type: new DataTypes.STRING(45),
      allowNull: false,
    },
    password: {
      type: new DataTypes.STRING(45),
      allowNull: false,
    },
  },
  {
    tableName: "user",
    sequelize,
  }
);

export default User;
